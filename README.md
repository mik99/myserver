# Myserver

## Introduction

̀̀Ce projet permet de déployer un serveur Linux. 

## Instructions

Clonez le projet dans votre dossier personnel:
```
git clone https://gitlab.com/mik99/myserver.git
```

Entrez maintenant dans le dossier télécharger:
    cd myserver

vous pouver maintenant:  
-installer un serveur web
-installer un sevreur ssh

### Installer un serveur web (Nginx)

Pour connaitre son adresse ip;
```
ip a 
```

On cherche la ligne qui contient inet(`ip a |grep inet`)

Ceci est l'adresse de notre ordinateur sur le réseau interne.

A ne pas confondre avec votre adresse ip publique,
qui est l'adresse de votre BOX, accessible depuis Internet.

Pour obtenir cette derniere, tapez "what is my ip ?" dans google.

##### Connaitre ses noms d'hôte

On utilise le fichier`/etc/hosts`,
qui fonction comme un server DNS: tableau de correspondance entre adresse ip et noms d'hotes.

ex:
```
127.0.0.1       localhost
127.0.1.1       kim.garage.gn   kim
```

##### Installer nginx

```
sudo apt install nginx
```

pour vérifier que le serveur fonctionne, on utilise: 
```
sudo systemectl status nginx
```
si inactive démarrer avec 
```
sudo systemectl start nginx

###### Modifier le contenu

La page par défaut d'accés à nginx se trouve à `/var/wwww/html/index.nginx-debian.html`

Modifier ce fichier pour changer la page d'accueil.

